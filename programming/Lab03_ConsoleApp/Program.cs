﻿using System;
using System.Net;
using System.IO;

namespace Lab03_ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var client = new WebClient())
            {
                var contents = client.DownloadString("https://auto.ria.com/uk/");
                string writePath = @"D:\Users\Downloads\AutoRia.html";
                try
                {
                    using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
                    {
                        sw.WriteLine(contents);
                    }
                    using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                    {
                        sw.WriteLine("Дозапись");
                        sw.Write(4.5);
                    }
                    Console.WriteLine("Запись выполнена");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Console.ReadKey();
        }
    }
}
